import React , { useState, useRef }  from "react";
import HeaderComponent from "../Header";
import Test from "../Test";

const Home =()=>{
    const [isScroll, setIsScroll] = useState(0);
    const myRef = useRef(0);
    const onScroll = () => {
      const scrollY = window.scrollY;
      const scrollTop = myRef.current.scrollTop;
      console.log(
        `onScroll, window.scrollY: ${scrollY} myRef.scrollTop: ${scrollTop}`
      );
      setIsScroll({
        scrollTop: scrollTop,
      });
    };
    return (
        <div  ref={myRef}
        onScroll={onScroll}>
            <HeaderComponent/>
             <Test/>
             <div
            
            style={{
            border: "1px solid black",
            width: "600px",
            height: "100px",
            overflow: "scroll",
            }}
              >
        <p>
          This demonstrates how to get the scrollTop position within a
          scrollable react component.
        </p>
        <p>
          This demonstrates how to get the scrollTop position within a
          scrollable react component.
        </p>
      </div>
        </div>
        
    )
}

export default Home;