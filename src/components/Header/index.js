import React from "react";
import { Nav, NavDropdown, Navbar } from "react-bootstrap";
import logo from "../../assets/image/logo.png";

import styled from "styled-components";

const Header = styled.div`
  background: black;
  height: 100px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  // position: fixed;
`;

const NavHeader = styled(Nav)`
  // justify-content: center
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;

  width: ${(props) => (props.primary ? "300px" : "")};
  justify-content: flex-end;
`;

const StyleLink = styled(Nav.Link)`
  color: #fff !important;
  padding-left: 2em;
  &:hover {
    color: #fa5c5d !important;
    transition: all 0.2s;
  }
`;

const StyleDropdown = styled(NavDropdown)`
  .nav-link {
    color: #fff !important;
    padding-left: 2em !important;
    &:hover {
      color: red !important;
      transition: all 0.2s;
    }
  }
`;

const Button = styled.button`
  background: no-repeat;
  color: #fa5c5d;
  border: 1px solid #fa5c5d;
  border-radius: 20px;
  padding: 10px 25px;
  font-weight: 500;
  &:hover {
    background-color: #fa5c5d;
    color: #fff;
    transition: all 0.2s;
  }
  @media only screen and (min-width: 768px) {
    display: none;
  }
  @media only screen and (min-width: 992px) {
    display: block;
  }
  @media only screen and (min-width: 1024px) {
    display: block;
  }
`;

const ButtonResponsive = styled.button`
  background-color: red;

  width: 5%;
  height: 30%;
  border: 0;
  padding: 9px 7px;
`;

const Span = styled.span`
  background-color: white;
  display: block;
  height: 2px;
  margin-bottom: 5px;
`;

// const scrollStickyOn = styled(Header)`
//   .header {
//     opacity: 1;
//     top: 0;
//     visibility: visible;
//   }
// `;
// const scrollSticky = styled(Header)`
//   .header {
//     width: 100%;
//     position: fixed;
//     left: 0;
//     top: 300px;
//     z-index: 9999;
//     opacity: 0;
//     visibility: hidden;
//   }
// `;

const NavResponsive = styled(Nav)`
  background: #fff;
  display: block !important;
  width: 300px;
  height: 100vh;
  border-left: 1px solid #cccccc;
  border-right: 1px solid #cccccc;
  position: fixed;
  right: 300px;
  top: 0;
  z-index: 100;
  text-align: left;
  opacity: 0;
  visibility: hidden;
`;

const NavItem = styled.div`
  border-bottom: 1px solid;
  padding: 7px 24px;
`;

const ButtonIcon = styled.button`
  position: absolute;
  border-radius: 16px;
  padding: 5px 13px;
  left: -20px;
  background-color: red;
  border: none;
  top: 12px;
  color: white;
`;

const HeaderComponent = () => {
  return (
    <div>
      <Header>
        <div className="container">
          <Navbar collapseOnSelect expand="lg" variant="dark">
            <Navbar.Brand href="#home">
              <img src={logo} alt="" />
            </Navbar.Brand>

            <NavHeader className="ml-auto menu">
              <StyleLink href="#features">Home</StyleLink>
              <StyleLink href="#pricing">About</StyleLink>
              <StyleLink href="#pricing">Project</StyleLink>
              <StyleLink href="#pricing">Contact</StyleLink>
              <StyleDropdown title="Blog" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Blog</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Blog Details
                </NavDropdown.Item>
              </StyleDropdown>
            </NavHeader>
            <NavHeader primary>
              <Nav.Link href="#deets">
                <Button>Get Started</Button>
              </Nav.Link>
            </NavHeader>
          </Navbar>
          <div className="manhinh"></div>
        </div>
      </Header>

      {/* responsive */}
      <div className="header">
        <div className="container">
          <Navbar collapseOnSelect expand="lg" variant="dark">
            <Navbar.Brand href="#home">
              <img src={logo} alt="" />
            </Navbar.Brand>
            <ButtonResponsive type="button" className="nut">
              <Span className="iconBar"></Span>
              <Span className="iconBar"></Span>
              <Span className="iconBar"></Span>
            </ButtonResponsive>
            <NavResponsive className="mr-auto manhinhtoi">
              <ButtonIcon>
                <span>X</span>
              </ButtonIcon>
              <NavItem href="#features">Home</NavItem>
              <NavItem href="#pricing">About</NavItem>
              <NavItem href="#pricing">Project</NavItem>
              <NavItem href="#pricing">Contact</NavItem>
              <StyleDropdown title="Blog" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Blog</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Blog Details
                </NavDropdown.Item>
              </StyleDropdown>
            </NavResponsive>
            {/* <NavHeader primary>
              <Nav.Link href="#deets">
                <Button>Get Started</Button>
              </Nav.Link>
            </NavHeader> */}
          </Navbar>
        </div>
      </div>
    </div>
  );
};
export default HeaderComponent;
